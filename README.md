<h1 align="center">Wopine</h1>

<p align="center">
    <pre align="center">
⣀⡀
⢠⣤⡀⣾⣿⣿⠀⣤⣤⡄
⢿⣿⡇⠘⠛⠁⢸⣿⣿⠃
⠈⣉⣤⣾⣿⣿⡆⠉⣴⣶⣶
⣾⣿⣿⣿⣿⣿⣿⡀⠻⠟⠃
⠙⠛⠻⢿⣿⣿⣿⡇
⠀⠀⠀⠀⠈⠙⠋⠁
</pre>
</p>

<p align="center">My config for <a href="https://hg.sr.ht/~scoopta/wofi">wofi</a>, w/ <a href="https://rosepinetheme.com/">Rose Pine</a> color scheme.</p>


<h2 align="center">Screenshots</h2>

#### Launcher
![img 1](img/1.png)

#### Power Menu
![img 2](img/2.png)
