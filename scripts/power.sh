#!/usr/bin/env bash

pwr=$(echo -e " poweroff\n restart\n suspend\n lock\n logout" | wofi -i --dmenu | awk '{print tolower($2)}')

case $pwr in 
    poweroff|reboot|suspend)
        systemctl $pwr
        ;;
    lock)
        swaylock
        ;;
    logout)
        swaymsg exit
        ;;
esac